# Linux4Space Yocto
This is the main repository for the yocto-based Linux distribution created within the Linux4Space project.

Detailed information can be found at www.linux4space.org.
The project is starting in February 2023, the first output shall be ready in the June 2023.

The main maintainer is Lenka Kosková Třísková, lenka.koskova.triskova@tul.cz

## Getting started
- git clone https://gitlab.com/linux4space/linux4space-yocto.git
- cd linux4space-yocto/
- git submodule update --init --recursive --remote
-  . poky/oe-init-env
- cp conf/local.conf.qemu conf/local.conf
- Adapt DL_DIR SSTATE_DIR TMPDIR to fit your needs
- bitbake core-image-minimal
- drink some beers
- runqemu core-image-minimal wic nographic ovmf slirp
- passwords: root:root admin:admin

## Layer structure

### meta-linux4space
The layer is dedicated to the OS configuration:
- The main distro configuration
- Image definitions
- Kernel related recipes
- Bootloader related recipes
- All the kernel-space applications
- System configuration

### meta-linux4space-app
The layer is dedicated to the user space applications, the recipes are part of the Linux4Space project.
(It means, that for all the included application it was verified or it is planned to verify the possible compliance with ECSS.)
The layer shall contain recipes and meta data for:
- The applications for the space use case
- F. e. the app responsible for creating the diagnostic reports
- Telemetry, navigation… all the stuff not requiring the kernel-space

### meta-space
The layer shall work as a container for other related Linux applications which may become usefull in space. 
The applications or other recipes are not part of the Linux4Space project.



